import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-pagination',
  templateUrl: './pagination.component.html',
  styleUrl: './pagination.component.css',
})
export class PaginationComponent {
  @Input() lastPage: number = 0;
  @Input() currentPage: number = 1;
  @Input() pageSize: number = 10;
  @Output() pageSizeChange = new EventEmitter<number>();
  @Output() pageChange = new EventEmitter<number>();

  pageSizeOptions: number[] = [5, 10, 25];
  onPageChanged(page: number): void {
    if (page >= 1 && page <= this.lastPage) {
      this.pageChange.emit(page);
    }
  }
  onChangePageSize(value: number): void {
    this.pageSize = value;
    this.pageSizeChange.emit(value);
  }

  getPages(): number[] {
    return Array.from({ length: this.lastPage }, (_, index) => index + 1);
  }
}
