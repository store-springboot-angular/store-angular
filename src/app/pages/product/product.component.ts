import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';
import { DataService } from '../../services/data.service';
@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css'],
})
export class ProductComponent implements OnInit {
  products: any[] = [];
  pagination: any = {};
  productHeaderTable: string[] = [
    'No',
    'Name Product ',
    'Price',
    'Stock',
    'Last Restock',
    'Status',
    'Action',
  ];
  productColumns: string[] = [
    'No',
    'itemName',
    'formattedProductPrice',
    'itemStock',
    'formattedProductLastRestock',
    'isAvailable',
  ];
  pageSize: number = 5;
  startIndex: number = 0;
  showModalDetail: boolean = false;
  showModalEdit: boolean = false;
  showModalDelete: boolean = false;
  showModalAdd: boolean = false;
  itemDetails: any;
  errorMessageProduct: string = '';
  searchForm!: FormGroup;
  searchResults: any[] = [];
  sortedData: any[] = [];
  currentSortColumn: string = '';
  isSortAsc: boolean = true;

  addProductForm = new FormGroup({
    itemName: new FormControl('', [
      Validators.required,
      Validators.minLength(3),
      Validators.maxLength(100),
      Validators.pattern(/^[a-zA-Z0-9\s]*$/),
    ]),
    itemStock: new FormControl('', [
      Validators.required,
      Validators.minLength(1),
      Validators.maxLength(10),
      Validators.pattern(/^[0-9]*$/),
    ]),
    itemPrice: new FormControl('', [
      Validators.required,
      Validators.minLength(3),
      Validators.maxLength(100),
      Validators.pattern(/^[0-9]*$/),
    ]),
  });

  updateProductForm = new FormGroup({
    itemName: new FormControl('', [
      Validators.required,
      Validators.minLength(3),
      Validators.maxLength(100),
      Validators.pattern(/^[a-zA-Z0-9\s]*$/),
    ]),
    itemStock: new FormControl('', [
      Validators.required,
      Validators.minLength(1),
      Validators.maxLength(10),
      Validators.pattern(/^[0-9]*$/),
    ]),
    itemPrice: new FormControl('', [
      Validators.required,
      Validators.minLength(3),
      Validators.maxLength(100),
      Validators.pattern(/^[0-9]*$/),
    ]),
  });

  constructor(private dataService: DataService) {
    this.searchForm = new FormGroup({
      search: new FormControl('', Validators.required),
    });
  }

  ngOnInit(): void {
    this.loadData(1);
    this.searchForm
      .get('search')!
      .valueChanges.pipe(
        debounceTime(300),
        distinctUntilChanged(),
        switchMap((value) => this.dataService.getItems(1, this.pageSize, value))
      )
      .subscribe((results) => {
        this.products = results.data.map((product: any) => ({
          ...product,
          formattedProductPrice: this.formatToRupiah(product.itemPrice),
          formattedProductLastRestock: this.formatToDateTime(
            product.itemLastRestock
          ),
          isAvailable: product.isAvailable ? 'AVAILABLE' : 'NOT AVAILABLE',
        }));
        this.pagination = results.pagination;
      });
  }

  loadData(page: number, sortOrder: string = ''): void {
    const startIndex = (page - 1) * this.pageSize;
    const searchValue = this.searchForm.get('search')!.value || '';

    this.dataService
      .getItems(page, this.pageSize, searchValue, sortOrder)
      .subscribe((response: any) => {
        this.products = response.data.map((product: any) => ({
          ...product,
          formattedProductPrice: this.formatToRupiah(product.itemPrice),
          formattedProductLastRestock: this.formatToDateTime(
            product.itemLastRestock
          ),
          isAvailable: product.isAvailable ? 'AVAILABLE' : 'NOT AVAILABLE',
        }));
        this.pagination = response.pagination;

        this.startIndex = startIndex;
      });
  }

  onSubmitAdd(): void {
    if (this.addProductForm.valid) {
      const formData = this.addProductForm.value;

      this.dataService.addItem(formData).subscribe(
        (response) => {
          alert('Product has been added:\n' + response.productName);
          this.addProductForm.reset();
          this.showModalAdd = false;
        },
        (error) => {
          alert('Product failed to add');
          console.error('Error adding Product', error);
        }
      );
    } else {
      this.addProductForm.markAllAsTouched();
    }
  }

  onSubmitEdit(): void {
    if (this.updateProductForm.valid) {
      const formData = {
        ...this.updateProductForm.value,
        itemId: this.itemDetails.itemId,
      };

      this.dataService.editItem(formData).subscribe(
        (response) => {
          alert('Product has been updated:\n' + response.productName);
          this.loadData(1);
          this.showModalEdit = false;
        },
        (error) => {
          alert('Product failed to update');
          console.error('Error updating Product', error);
        }
      );
    } else {
      this.updateProductForm.markAllAsTouched();
    }
  }

  onSubmitDelete(): void {
    if (this.itemDetails) {
      this.dataService.deleteItem(this.itemDetails).subscribe(
        () => {
          alert('Product has been deleted:\n' + this.itemDetails.itemName);
          this.loadData(1);
          this.showModalDelete = false;
        },
        (error) => {
          alert('Product failed to delete');
          console.error('Error deleting Product', error);
        }
      );
    }
  }

  onSort({ column, direction }: { column: string; direction: 'asc' | 'desc' }) {
    this.currentSortColumn = column;
    this.isSortAsc = direction === 'asc';

    const joinSort =
      column + direction.charAt(0).toUpperCase() + direction.slice(1);
    const sortVariable = joinSort
      .replace(/([a-z])([A-Z])/g, '$1_$2')
      .toUpperCase();

    this.loadData(1, sortVariable);
  }

  formatToRupiah(amount: number): string {
    const formatter = new Intl.NumberFormat('id-ID', {
      style: 'currency',
      currency: 'IDR',
      minimumFractionDigits: 2,
    });
    return formatter.format(amount).replace('IDR', 'Rp.');
  }

  formatToDateTime(dateTime: string): string {
    const date = new Date(dateTime);
    return date.toLocaleString('id-ID', { timeZone: 'UTC' });
  }

  onPageChanged(page: number): void {
    let sortOrder = '';
    if (this.currentSortColumn) {
      const direction = this.isSortAsc ? 'asc' : 'desc';
      const joinSort = this.currentSortColumn + '_' + direction.toUpperCase();
      sortOrder = joinSort.replace(/([a-z])([A-Z])/g, '$1_$2').toUpperCase();
    }
    this.loadData(page, sortOrder);
  }

  onPageSizeChanged(pageSize: number): void {
    let sortOrder = '';
    if (this.currentSortColumn) {
      const direction = this.isSortAsc ? 'asc' : 'desc';
      const joinSort = this.currentSortColumn + '_' + direction.toUpperCase();
      sortOrder = joinSort.replace(/([a-z])([A-Z])/g, '$1_$2').toUpperCase();
    }

    this.pageSize = pageSize;
    this.loadData(1, sortOrder);
  }

  onDetail(product: any) {
    this.itemDetails = product;
    this.showModalDetail = true;
  }

  onAdd() {
    this.showModalAdd = true;
  }

  onEdit(item: any) {
    this.itemDetails = item;
    this.updateProductForm.patchValue({
      itemName: item.itemName,
      itemStock: item.itemStock,
      itemPrice: item.itemPrice,
    });
    this.showModalEdit = true;
  }

  onDelete(product: any) {
    this.itemDetails = product;
    this.showModalDelete = true;
  }
  hideModal(): void {
    this.resetForm();
    this.showModalDetail = false;
    this.showModalEdit = false;
    this.showModalDelete = false;
    this.showModalAdd = false;
  }

  resetForm() {
    this.addProductForm.reset();
    this.updateProductForm.reset();
  }

  get itemName() {
    return this.addProductForm.get('itemName');
  }
  get itemPrice() {
    return this.addProductForm.get('itemPrice');
  }
  get itemStock() {
    return this.addProductForm.get('itemStock');
  }

  get itemNameUpdate() {
    return this.updateProductForm.get('itemName');
  }
  get itemPriceUpdate() {
    return this.updateProductForm.get('itemPrice');
  }
  get itemStockUpdate() {
    return this.updateProductForm.get('itemStock');
  }
}
