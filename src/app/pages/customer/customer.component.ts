import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';
import { DataService } from '../../services/data.service';
@Component({
  selector: 'app-customer',
  templateUrl: './customer.component.html',
  styleUrls: ['./customer.component.css'],
})
export class CustomerComponent implements OnInit {
  customers: any[] = [];
  pagination: any = {};
  customerHeaderTable: string[] = [
    'No',
    'Picture',
    'Name',
    'Address',
    'Phone Number',
    'Action',
  ];
  customerColumns: string[] = [
    'No',
    'picture',
    'customerName',
    'customerAddress',
    'phoneNumber',
  ];
  pageSize: number = 5;
  startIndex: number = 0;
  condition: boolean = true;
  showModalDetail: boolean = false;
  showModalEdit: boolean = false;
  showModalDelete: boolean = false;
  showModalAdd: boolean = false;
  customerDetails: any;
  errorMessageCustomer: string = '';
  searchForm!: FormGroup;
  searchResults: any[] = [];
  selectedFile: File | null = null;

  sortedData: any[] = [];
  currentSortColumn: string = '';
  isSortAsc: boolean = true;

  addCustomerForm = new FormGroup({
    customerName: new FormControl('', [
      Validators.required,
      Validators.minLength(3),
      Validators.maxLength(20),
      Validators.pattern(/^[a-zA-Z\s]*$/),
    ]),
    customerAddress: new FormControl('', [
      Validators.required,
      Validators.minLength(5),
      Validators.maxLength(200),
    ]),
    phoneNumber: new FormControl('', [
      Validators.required,
      Validators.minLength(11),
      Validators.maxLength(12),
      Validators.pattern(/^[0-9]*$/),
    ]),
    picture: new FormControl('', [Validators.required]),
  });

  updateCustomerForm = new FormGroup({
    customerName: new FormControl('', [
      Validators.required,
      Validators.minLength(3),
      Validators.maxLength(20),
      Validators.pattern(/^[a-zA-Z\s]*$/),
    ]),
    customerAddress: new FormControl('', [
      Validators.required,
      Validators.minLength(5),
      Validators.maxLength(200),
    ]),
    phoneNumber: new FormControl('', [
      Validators.required,
      Validators.minLength(11),
      Validators.maxLength(12),
      Validators.pattern(/^[0-9]*$/),
    ]),
  });

  constructor(private dataService: DataService) {
    this.searchForm = new FormGroup({
      search: new FormControl('', Validators.required),
    });
  }

  ngOnInit(): void {
    this.loadData(1);
    this.searchForm
      .get('search')!
      .valueChanges.pipe(
        debounceTime(300),
        distinctUntilChanged(),
        switchMap((value) =>
          this.dataService.getCustomers(1, this.pageSize, value)
        )
      )
      .subscribe((results) => {
        this.customers = results.data;
        this.pagination = results.pagination;
      });
  }

  loadData(page: number, sortOrder: string = ''): void {
    const startIndex = (page - 1) * this.pageSize;
    const searchValue = this.searchForm.get('search')!.value || '';

    this.dataService
      .getCustomers(page, this.pageSize, searchValue, sortOrder)
      .subscribe((response: any) => {
        this.customers = response.data;
        this.pagination = response.pagination;

        this.startIndex = startIndex;
      });
  }

  onSubmitAdd(): void {
    if (this.addCustomerForm.valid && this.selectedFile) {
      const formData = new FormData();
      const formValue = this.addCustomerForm.value;

      if (
        formValue.customerName !== null &&
        formValue.customerName !== undefined
      ) {
        formData.append('customerName', formValue.customerName);
      }
      if (
        formValue.customerAddress !== null &&
        formValue.customerAddress !== undefined
      ) {
        formData.append('customerAddress', formValue.customerAddress);
      }
      if (
        formValue.phoneNumber !== null &&
        formValue.phoneNumber !== undefined
      ) {
        formData.append('phoneNumber', formValue.phoneNumber);
      }
      formData.append('picture', this.selectedFile);
      this.dataService.addCustomer(formData).subscribe(
        (response) => {
          alert(
            'Customer data added successfully:\n' +
              'Name: ' +
              response.customer.customerName +
              '\n' +
              'Address: ' +
              response.customer.customerAddress +
              '\n' +
              'Phone Number: ' +
              response.customer.phoneNumber
          );

          this.addCustomerForm.reset();
          this.hideModal();
          window.location.reload();
        },
        (error) => {
          alert('Customer data failed to add');
          console.error('Error adding customer', error);
        }
      );
    } else {
      this.addCustomerForm.markAllAsTouched();
    }
  }

  onSubmitEdit(): void {
    if (this.updateCustomerForm && this.updateCustomerForm.valid) {
      const customerId = this.customerDetails.customerId;

      const formData = new FormData();
      formData.append('customerId', customerId.toString());

      const firstNameValue = this.updateCustomerForm.get('customerName')?.value;
      const lastNameValue =
        this.updateCustomerForm.get('customerAddress')?.value;
      const phoneNumberValue =
        this.updateCustomerForm.get('phoneNumber')?.value;

      if (firstNameValue) {
        formData.append('customerName', firstNameValue);
      }
      if (lastNameValue) {
        formData.append('customerAddress', lastNameValue);
      }
      if (phoneNumberValue) {
        formData.append('phoneNumber', phoneNumberValue);
      }

      if (this.selectedFile) {
        formData.append('picture', this.selectedFile);
      }

      this.dataService.editCustomer(customerId, formData).subscribe(
        (response) => {
          alert('Customer updated successfully:\n');
          this.loadData(1);
          this.showModalEdit = false;
        },
        (error) => {
          alert('Customer failed to update');
          console.error('Error updating customer', error);
        }
      );
    } else {
      if (this.updateCustomerForm) {
        this.updateCustomerForm.markAllAsTouched();
      }
    }
  }

  onSubmitDelete(): void {
    if (this.customerDetails) {
      this.dataService.deleteCustomer(this.customerDetails).subscribe(
        () => {
          this.loadData(1);
          this.showModalDelete = false;
        },
        (error) => {
          console.error('Error deleting customer', error);
        }
      );
    }
  }

  onSort({ column, direction }: { column: string; direction: 'asc' | 'desc' }) {
    this.currentSortColumn = column;
    this.isSortAsc = direction === 'asc';

    const joinSort =
      column + direction.charAt(0).toUpperCase() + direction.slice(1);
    const sortVariable = joinSort
      .replace(/([a-z])([A-Z])/g, '$1_$2')
      .toUpperCase();

    this.loadData(1, sortVariable);
  }

  onFileSelected(event: any): void {
    if (event.target.files && event.target.files.length > 0) {
      this.selectedFile = event.target.files[0];
    }
  }

  onPageChanged(page: number): void {
    let sortOrder = '';
    if (this.currentSortColumn) {
      const direction = this.isSortAsc ? 'asc' : 'desc';
      const joinSort = this.currentSortColumn + '_' + direction.toUpperCase();
      sortOrder = joinSort.replace(/([a-z])([A-Z])/g, '$1_$2').toUpperCase();
    }

    this.loadData(page, sortOrder);
  }

  onPageSizeChanged(pageSize: number): void {
    let sortOrder = '';
    if (this.currentSortColumn) {
      const direction = this.isSortAsc ? 'asc' : 'desc';
      const joinSort = this.currentSortColumn + '_' + direction.toUpperCase();
      sortOrder = joinSort.replace(/([a-z])([A-Z])/g, '$1_$2').toUpperCase();
    }
    this.pageSize = pageSize;
    this.loadData(1, sortOrder);
  }

  onDetail(customer: any) {
    this.customerDetails = customer;
    this.showModalDetail = true;
  }

  onEdit(customer: any) {
    this.customerDetails = customer;
    this.updateCustomerForm.patchValue({
      customerName: customer.customerName,
      customerAddress: customer.customerAddress,
      phoneNumber: customer.phoneNumber,
    });
    this.showModalEdit = true;
  }

  onDelete(customer: any) {
    this.customerDetails = customer;
    this.showModalDelete = true;
  }

  onAdd() {
    this.showModalAdd = true;
  }

  hideModal(): void {
    this.resetForm();
    this.showModalDetail = false;
    this.showModalEdit = false;
    this.showModalDelete = false;
    this.showModalAdd = false;
  }

  get customerName() {
    return this.addCustomerForm.get('customerName');
  }

  get customerAddress() {
    return this.addCustomerForm.get('customerAddress');
  }

  get phoneNumber() {
    return this.addCustomerForm.get('phoneNumber');
  }

  resetForm() {
    this.addCustomerForm.reset();
    this.updateCustomerForm.reset();
  }

  get customerNameUpdate() {
    return this.updateCustomerForm.get('customerName');
  }

  get customerAddressUpdate() {
    return this.updateCustomerForm.get('customerAddress');
  }

  get phoneNumberUpdate() {
    return this.updateCustomerForm.get('phoneNumber');
  }
}
