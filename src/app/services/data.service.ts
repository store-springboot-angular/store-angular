import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '../../environment/environment';

@Injectable({
  providedIn: 'root',
})
export class DataService {
  private customersUrl = `${environment.apiUrl}/customers`;
  private customerUrl = `${environment.apiUrl}/customer`;
  private itemsUrl = `${environment.apiUrl}/items`;
  private itemUrl = `${environment.apiUrl}/item`;
  private ordersUrl = `${environment.apiUrl}/orders`;
  private orderUrl = `${environment.apiUrl}/order`;
  private orderDownloadUrl = `${environment.apiUrl}/report/pdf`;

  constructor(private http: HttpClient) {}

  getCustomers(
    pageNo: number,
    pageSize: number,
    customerName: String = '',
    sortOrder: string = ''
  ): Observable<any> {
    const url = `${this.customersUrl}?pageNo=${pageNo}&pageSize=${pageSize}&customerName=${customerName}&sortOrder=${sortOrder}`;
    return this.http.get(url);
  }

  addCustomer(customer: any): Observable<any> {
    return this.http.post(this.customerUrl, customer);
  }

  editCustomer(customerId: number, customer: any): Observable<any> {
    const url = `${this.customerUrl}/${customerId}`;
    return this.http.put(url, customer);
  }

  deleteCustomer(customer: any): Observable<any> {
    const url = `${this.customerUrl}/${customer.customerId}`;
    return this.http.delete(url);
  }

  getItems(
    page: number,
    pageSize: number,
    searchValue: string = '',
    sortOrder: string = ''
  ): Observable<any> {
    const url = `${this.itemsUrl}?pageNo=${page}&pageSize=${pageSize}&itemName=${searchValue}&sortOrder=${sortOrder}`;
    return this.http.get(url);
  }

  addItem(item: any): Observable<any> {
    return this.http.post(this.itemUrl, item);
  }

  editItem(item: any): Observable<any> {
    const url = `${this.itemUrl}/${item.itemId}`;
    return this.http.put(url, item);
  }

  deleteItem(item: any): Observable<any> {
    const url = `${this.itemUrl}/${item.itemId}`;
    return this.http.delete(url);
  }

  getOrders(
    pageNo: number,
    pageSize: number,
    customerName: String = '',
    sortOrder: string = ''
  ): Observable<any> {
    const url = `${this.ordersUrl}?pageNo=${pageNo}&pageSize=${pageSize}&customerName=${customerName}&sortOrder=${sortOrder}`;
    return this.http.get(url);
  }

  addOrder(order: any): Observable<any> {
    return this.http.post(this.orderUrl, order);
  }

  editOrder(orderId: number, order: any): Observable<any> {
    return this.http.put<any>(`${this.orderUrl}/${orderId}`, order);
  }

  deleteOrder(order: any): Observable<any> {
    const url = `${this.orderUrl}/${order.orderId}`;
    return this.http.delete(url);
  }

  downloadOrder(): Observable<Blob> {
    const url = `${this.orderDownloadUrl}`;
    return this.http.get(url, { responseType: 'blob' });
  }
}
